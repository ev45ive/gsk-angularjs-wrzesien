// create module
var app = angular.module('app.module', [
  'todos.module',
  'users.module',
]);

app.constant('defaultTodos', [{
  id: 1,
  title: 'Get Angular!',
  completed: false
}])

app.config(function () {})

app.run(function () {})

app.controller('appCtrl', function ($scope) {
  $scope.name = 'Angular App';

  $scope.user = {
    name: "Alice The User"
  }

  $scope.activeView = 'users'

  $scope.activateView = function(name){
    $scope.activeView = name
  }

})