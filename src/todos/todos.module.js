var todos = angular.module('todos.module', [])

todos.controller('todosCtrl', function ($scope, Todos) {
  function createTodo() {
    return {
      id: 0,
      title: '',
      completed: false
    }
  }

  $scope.todo = createTodo()

  Todos.fetchTodos()
      .then(function(todos){
        $scope.todos = todos
      })


  $scope.removeTodo = function (index) {
    $scope.todos.splice(index, 1)
  }

  $scope.addTodo = function (todo) {
    todo.id = Date.now()
    // todo = angular.copy(todo)
    $scope.todos.push(todo)
    $scope.todo = createTodo()
  }

  $scope.titleKey = function (event) {
    if (event.keyCode == 13) {
      $scope.addTodo($scope.todo)
    }
  }
})