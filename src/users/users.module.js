// Users module
var users = angular.module('users.module', [])

// Users controller
users.controller('usersCtrl', function ($scope, UsersService) {

  $scope.selectedUser = {};
  $scope.editingUser = null;

  $scope.select = function (user) {
    $scope.selectedUser = $scope.selectedUser == user ? null : user;
  }

  $scope.editMode = false;

  $scope.edit = function () {
    $scope.editMode = true;
    $scope.editingUser = angular.copy($scope.selectedUser)
  }

  $scope.save = function () {
    if ($scope.userForm.$invalid) {
      return
    }
    UsersService.saveUser($scope.editingUser)
      .then(function () {
        return UsersService.fetchUsers()
      })
      .then(function (users) {
        $scope.users = users
        $scope.select($scope.editingUser)
        $scope.editMode = false;
      })
  }

  $scope.cancel = function () {
    $scope.editMode = false;
  }

  $scope.users = []

  UsersService.fetchUsers()
    .then(function (users) {
      $scope.users = users
    })

})